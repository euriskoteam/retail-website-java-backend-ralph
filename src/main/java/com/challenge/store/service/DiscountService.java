package com.challenge.store.service;

import java.util.List;

import com.challenge.store.model.bill.Bill;
import com.challenge.store.model.item.Item;
import com.challenge.store.model.item.ItemCategory;
import com.challenge.store.service.DiscountCalculation;

public class DiscountService {

	/*
	 * This method calculates and return the discounted total bill of the user
	 */
	public Double calculateTotal(Bill bill) {
		double total = 0;
		double priceForGroceries = 0;
		double priceForNonGroceries = 0;
		double totalPercentageDiscount = 0;

		List<Item> items = bill.getItems();
		for (Item item : items) {
			if (item.getCategory() == ItemCategory.GROCERIES) {
				priceForGroceries += item.getPrice();
			} else {
				priceForNonGroceries += item.getPrice();
			}
		}

		totalPercentageDiscount = DiscountCalculation.calculatePercentageBasedDiscount(priceForNonGroceries, bill);

		total = totalPercentageDiscount + priceForGroceries;

		return total - DiscountCalculation.calculateAmountBasedDiscount(total);
	}
}
