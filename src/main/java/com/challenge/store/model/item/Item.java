package com.challenge.store.model.item;

public class Item {
	private String id;
	private String name;
	private ItemCategory category;
	private Double price;
	
	/**
	 * @param id
	 * @param name
	 * @param category
	 * @param price
	 */
	public Item(String id, String name, ItemCategory category, Double price) {
		this.id = id;
		this.name = name;
		this.category = category;
		this.price = price;
	}
	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the category
	 */
	public ItemCategory getCategory() {
		return category;
	}
	/**
	 * @param category the category to set
	 */
	public void setCategory(ItemCategory category) {
		this.category = category;
	}
	/**
	 * @return the price
	 */
	public Double getPrice() {
		return price;
	}
	/**
	 * @param price the price to set
	 */
	public void setPrice(Double price) {
		this.price = price;
	}
	
}
