package com.challenge.store;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.challenge.store.model.bill.Bill;
import com.challenge.store.model.item.Item;
import com.challenge.store.model.item.ItemCategory;
import com.challenge.store.model.user.User;
import com.challenge.store.model.user.UserCategory;
import com.challenge.store.service.DiscountService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class DiscountServiceTest {

	private DiscountService billCalculator = new DiscountService();

	/*
	 * This method test if the discount on the total bill is applied correctly On an
	 * Affiliate user bill
	 */
	@Test
	public void affiliateTotalBillDiscount() {
		User affiliate = new User("id0", "fullName", new Date(), UserCategory.AFFILIATE);

		ArrayList<Item> items = new ArrayList<>();
		items.add(new Item("id0", "item0", ItemCategory.SPORTS, 75.0));
		items.add(new Item("id1", "item1", ItemCategory.GROCERIES, 20.0));
		items.add(new Item("id2", "item2", ItemCategory.OTHER, 37.0));

		Bill affiliateBill = new Bill("id0", items, affiliate);

		assertEquals(115.8, billCalculator.calculateTotal(affiliateBill), 0);
	}

	/*
	 * This method test if the discount on the total bill is applied correctly On an
	 * Employee user bill
	 */
	@Test
	public void employeeTotalBillDiscount() {
		User employee = new User("id1", "fullName", new Date(), UserCategory.EMPLOYEE);

		ArrayList<Item> items = new ArrayList<>();
		items.add(new Item("id0", "item0", ItemCategory.ELECTRONICS, 85.0));
		items.add(new Item("id1", "item1", ItemCategory.MOBILES, 45.0));
		items.add(new Item("id2", "item2", ItemCategory.GROCERIES, 25.0));

		Bill employeeBill = new Bill("id1", items, employee);

		assertEquals(111.0, billCalculator.calculateTotal(employeeBill), 0);
	}

	/*
	 * This method test if the discount on the total bill is applied correctly On an
	 * old customer who has been visiting the store for over 2 years
	 */
	@Test
	public void oldCustomerTotalBillDiscount() {
		Calendar calendar = new GregorianCalendar(2016, 2, 15);
		Date date = calendar.getTime();
		User customer = new User("id2", "fullName", date, UserCategory.CUSTOMER);

		ArrayList<Item> items = new ArrayList<>();
		items.add(new Item("id0", "item0", ItemCategory.ELECTRONICS, 30.0));
		items.add(new Item("id1", "item1", ItemCategory.MOBILES, 50.0));
		items.add(new Item("id2", "item2", ItemCategory.SPORTS, 25.0));
		items.add(new Item("id3", "item3", ItemCategory.GROCERIES, 15.0));
		items.add(new Item("id4", "item4", ItemCategory.OTHER, 20.0));

		Bill customerBill = new Bill("id2", items, customer);

		assertEquals(128.75, billCalculator.calculateTotal(customerBill), 0);
	}

	/*
	 * This method test if the discount on the total bill is applied correctly On a
	 * new customer where the visits to the store are less than 2 years
	 */
	@Test
	public void newCustomerTotalBillDiscount() {
		Calendar calendar = new GregorianCalendar(2018, 12, 25);
		Date date = calendar.getTime();
		User customer = new User("id3", "fullName", date, UserCategory.CUSTOMER);

		ArrayList<Item> items = new ArrayList<>();
		items.add(new Item("id0", "item0", ItemCategory.ELECTRONICS, 15.0));
		items.add(new Item("id1", "item1", ItemCategory.MOBILES, 35.0));
		items.add(new Item("id2", "item2", ItemCategory.SPORTS, 55.0));
		items.add(new Item("id3", "item3", ItemCategory.GROCERIES, 20.0));

		Bill customerBill = new Bill("id3", items, customer);

		assertEquals(120.0, billCalculator.calculateTotal(customerBill), 0);
	}
}
