package com.challenge.store.model.user;

import java.util.Date;

public class User {
	private String id;
	private String name;
	private Date created;
	private UserCategory category;
	
	/**
	 * @param id
	 * @param name
	 * @param created
	 * @param category
	 */
	public User(String id, String name, Date created, UserCategory category) {
		this.id = id;
		this.name = name;
		this.created = created;
		this.category = category;
	}
	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the created
	 */
	public Date getCreated() {
		return created;
	}
	/**
	 * @param created the created to set
	 */
	public void setCreated(Date created) {
		this.created = created;
	}
	/**
	 * @return the category
	 */
	public UserCategory getCategory() {
		return category;
	}
	/**
	 * @param category the category to set
	 */
	public void setCategory(UserCategory category) {
		this.category = category;
	}
	
}
