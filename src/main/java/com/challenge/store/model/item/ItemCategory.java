package com.challenge.store.model.item;

public enum ItemCategory {
	ELECTRONICS, MOBILES, SPORTS, GROCERIES, OTHER
}
