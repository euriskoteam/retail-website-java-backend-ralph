package com.challenge.store.model.bill;

import java.util.ArrayList;

import com.challenge.store.model.item.Item;
import com.challenge.store.model.user.User;

public class Bill {
	private String id;
	private ArrayList<Item> items = new ArrayList<>();
	private User user;

	/**
	 * @param id
	 * @param items
	 * @param user
	 */
	public Bill(String id, ArrayList<Item> items, User user) {
		this.id = id;
		this.items = items;
		this.user = user;
	}
	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * @return the items
	 */
	public ArrayList<Item> getItems() {
		return items;
	}
	/**
	 * @param items the items to set
	 */
	public void setItems(ArrayList<Item> items) {
		this.items = items;
	}
	/**
	 * @return the user
	 */
	public User getUser() {
		return user;
	}
	/**
	 * @param user the user to set
	 */
	public void setUser(User user) {
		this.user = user;
	}

}
