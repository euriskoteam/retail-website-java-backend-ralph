package com.challenge.store.service;

import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneId;
import java.util.Date;

import com.challenge.store.model.bill.Bill;

public class DiscountCalculation {

	/*
	 * This method calculate the percentage based discount Given the total of non
	 * grocery items and the user category
	 */
	public static Double calculatePercentageBasedDiscount(Double price, Bill bill) {
		switch (bill.getUser().getCategory()) {
		case EMPLOYEE:
			price -= price * 0.3;
			break;
		case AFFILIATE:
			price -= price * 0.1;
			break;
		case CUSTOMER:
			price -= calculateCustomerVisits(price, bill.getUser().getCreated());
			break;
		}
		return price;
	}

	/*
	 * This method is called if the user is a customer It calculates how many years
	 * the customer has been visiting the store If it's more than 2 years, a 5%
	 * discount will be applied to his bill
	 */
	public static Double calculateCustomerVisits(Double price, Date created) {
		LocalDate current = LocalDate.now();
		LocalDate userDate = created.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		Period period = Period.between(userDate, current);
		int diffInYears = period.getYears();
		return diffInYears >= 2 ? price * 0.05 : 0;
	}

	/*
	 * This method is to calculate the amount based discount For every $100 on the
	 * bill, a $5 discount will be applied
	 */
	public static double calculateAmountBasedDiscount(double total) {
		return (int) (total / 100) * 5;
	}
}
