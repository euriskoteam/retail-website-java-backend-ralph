# Retail Store Discounts

The Retail Store Discounts is a Java Spring application that calculates the discounts on the total bill of different users. The below guidelines will help you install and run the test environment.

## Installation

Clone the repository by running:

```bash
git clone https://RalphJbeily@bitbucket.org/euriskoteam/retail-website-java-backend-ralph.git
```

Navigate to the root directory of the project and install all the packages by running:

```bash
mvn install
```

## Testing

To execute the test cases against the business logic service run the following command:

```bash
mvn test
```

## UML Class Diagram

The UML Class Diagram defining the project model can be found [here](class_diagram.ucls)

![class_diagram](class_diagram.png)
