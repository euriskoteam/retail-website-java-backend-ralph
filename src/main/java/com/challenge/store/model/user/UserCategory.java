package com.challenge.store.model.user;

public enum UserCategory {
	EMPLOYEE, AFFILIATE, CUSTOMER
}
